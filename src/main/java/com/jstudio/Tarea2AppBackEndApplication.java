package com.jstudio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea2AppBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tarea2AppBackEndApplication.class, args);
	}
}
