package com.jstudio.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jstudio.model.Producto;

@Repository
public interface IProductoDao extends JpaRepository<Producto, Integer>{

}
