package com.jstudio.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jstudio.model.DetalleVenta;

@Repository
public interface IDetalleVentaDao extends JpaRepository<DetalleVenta, Integer>{

}
