package com.jstudio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jstudio.dao.IPersonaDao;
import com.jstudio.model.Persona;
import com.jstudio.service.IPersonaService;

@Service
public class PersonaServiceImpl implements IPersonaService{

	@Autowired
	private IPersonaDao dao;
	
	@Override
	public Persona registrar(Persona persona) {
		return dao.save(persona);
	}

	@Override
	public void actualizar(Persona persona) {
		dao.save(persona);
	}

	@Override
	public void eliminar(int idPersona) {
		dao.delete(idPersona);		
	}

	@Override
	public Persona listarId(int idPersona) {
		return dao.findOne(idPersona);
	}

	@Override
	public List<Persona> listar() {
		return dao.findAll();
	}

}
