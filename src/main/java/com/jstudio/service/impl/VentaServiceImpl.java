package com.jstudio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jstudio.dao.IVentaDao;
import com.jstudio.model.DetalleVenta;
import com.jstudio.model.Venta;
import com.jstudio.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	private IVentaDao dao;
	
	@Override
	public Venta registrar(Venta venta) {
		for(DetalleVenta det : venta.getDetalleVentas()) {
			det.setVenta(venta);
		}
		return dao.save(venta);
	}

	@Override
	public void actualizar(Venta venta) {
		dao.save(venta);
	}

	@Override
	public void eliminar(int idVenta) {
		dao.delete(idVenta);
	}

	@Override
	public Venta listarId(int idVenta) {
		return dao.findOne(idVenta);
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}

}
