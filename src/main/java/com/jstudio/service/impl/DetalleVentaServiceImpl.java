package com.jstudio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jstudio.dao.IDetalleVentaDao;
import com.jstudio.model.DetalleVenta;
import com.jstudio.service.IDetalleVentaService;

@Service
public class DetalleVentaServiceImpl implements IDetalleVentaService{

	@Autowired
	private IDetalleVentaDao dao;
	
	@Override
	public DetalleVenta registrar(DetalleVenta detalleVenta) {
		return dao.save(detalleVenta);
	}

	@Override
	public void actualizar(DetalleVenta detalleVenta) {
		dao.save(detalleVenta);
	}

	@Override
	public void eliminar(int idDetalleVenta) {
		dao.delete(idDetalleVenta);
	}

	@Override
	public DetalleVenta listarId(int idDetalleVenta) {
		return dao.findOne(idDetalleVenta);
	}

	@Override
	public List<DetalleVenta> listar() {
		return dao.findAll();
	}

}
