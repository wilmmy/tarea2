package com.jstudio.service;

import java.util.List;

import com.jstudio.model.DetalleVenta;

public interface IDetalleVentaService {
	
	DetalleVenta registrar(DetalleVenta detalleVenta);
	
	void actualizar(DetalleVenta detalleVenta);
	
	void eliminar(int idDetalleVenta);
	
	DetalleVenta listarId(int idDetalleVenta);
	
	List<DetalleVenta> listar();

}
