package com.jstudio.service;

import java.util.List;

import com.jstudio.model.Venta;

public interface IVentaService {
	
	Venta registrar(Venta venta);
	
	void actualizar(Venta venta);
	
	void eliminar(int idVenta);
	
	Venta listarId(int idVenta);
	
	List<Venta> listar();

}
