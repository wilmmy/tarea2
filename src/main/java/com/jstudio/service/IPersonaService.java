package com.jstudio.service;

import java.util.List;

import com.jstudio.model.Persona;

public interface IPersonaService {
	
	Persona registrar(Persona persona);
	
	void actualizar(Persona persona);
	
	void eliminar(int idPersona);
	
	Persona listarId(int idPersona);
	
	List<Persona> listar();

}
