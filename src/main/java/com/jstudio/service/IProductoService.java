package com.jstudio.service;

import java.util.List;

import com.jstudio.model.Producto;

public interface IProductoService {
	
	Producto registrar(Producto producto);
	
	void actualizar(Producto producto);
	
	void eliminar(int idProducto);
	
	Producto listarId(int idProducto);
	
	List<Producto> listar();

}
